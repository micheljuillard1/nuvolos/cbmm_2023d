var y a k c i h;
varexo e;

parameters theta rho eta gamma beta delta aa;

beta = 0.99;
delta = 0.025;
theta = 0.2;
rho = 0.9959; 
eta = 1.0051;  
gamma = 0.0045;
aa = 1.8;

model;
y = a*k(-1)^theta*h^(1-theta);
log(a) = (1-rho)*log(aa)+rho*log(a(-1)) + e;
y = c + i;
eta*k = (1-delta)*k(-1) + i;
gamma*c*h = (1-theta)*y;
eta/c = beta*(1/c(+1))*(theta*(y(+1)/k)+1-delta);
end;

steady_state_model;
  a = aa;
  y = a^(1/(1-theta))*(theta/(eta/beta - 1 + delta))^(theta/(1-theta))*((1-theta)/gamma)/(1 - theta*(eta - 1 + delta)/(eta/beta - 1 + delta));
  k = theta/(eta/beta - 1 + delta)*y;
  i = (eta - 1 + delta)*k;
  c = y - i;
  h = ((1 - theta)/gamma)/(1 - theta*(eta - 1 + delta)/(eta/beta - 1 + delta));
end;

steady;
