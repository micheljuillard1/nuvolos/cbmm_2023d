using Dynare

# can't find mode in original model
#context1 = @dynare "nk1a"
##
# mode found for model with tighter priors
context2 = @dynare "nk2";
##
# use this mode as initial values for original model 
context3 = @dynare "nk";
mode_compute!(datafile="dsge1_data.csv", context=context3, initial_values=context2.results.model_results[1].estimation.posterior_mode);
# doesn't work without transforming coeffcients
# context4 = deepcopy(context3)
##
#mode_compute!(datafile="dsge1_data.csv", context=context4, initial_values=context2.results.model_results[1].estimation.posterior_mode,transformed_parameters=false)
covariance = context3.results.model_results[1].estimation.posterior_mode_covariance;
##
initial_values = context3.results.model_results[1].estimation.posterior_mode;
rwmh_compute!(context = context3, 
              datafile="dsge1_data.csv",
              initial_values = initial_values,
              covariance = covariance,
              mcmc_jscale = 0.2,
              mcmc_replic = 30000,);
##
chain = rwmh_compute!(context=context3, datafile="dsge1_data.csv", mcmc_jscale=0.025, mcmc_replic=100000)
newcovariance = Dynare.covariance(chain)
initial_values = chain.value[end, 1:end-1]
chain2 = rwmh_compute!(context=context3, datafile="dsge1_data.csv", mcmc_jscale=0.025, mcmc_replic=100000, covariance = newcovariance)

##
initial_values = context3.results.model_results[1].estimation.posterior_mode;
rwmh_compute!(context = context3, 
              datafile="dsge1_data.csv",
              initial_values = initial_values,
              covariance = covariance,
              mcmc_jscale = 0.2,
              mcmc_replic = 30000,
              transformed_parameters = false);
##
chain = rwmh_compute!(context=context3, datafile="dsge1_data.csv", mcmc_jscale=0.025, mcmc_replic=100000, transformed_parameters = false)
newcovariance = Dynare.covariance(chain)
initial_values = chain.value[end, 1:end-1]
##
chain3 = rwmh_compute!(context=context3, 
                       datafile="dsge1_data.csv", 
                       mcmc_jscale=0.025, 
                       mcmc_replic=100000,
                       initial_values = initial_values, 
                       covariance = newcovariance, 
                       transformed_parameters = false, 
                       mcmc_chains=4)

nothing
