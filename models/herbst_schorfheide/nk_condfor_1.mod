var y R pi z g YGR INFL INT;
varexo eg ep ez;

parameters tau rho_R psi1 psi2 rho_g rho_z kappa
           piA gammaQ rA s_ep s_eg s_ez; 

rA = 0.42;
piA = 3.3;
gammaQ = 0.52;
tau = 2.83;
kappa = 0.78;
psi1 = 1.8;
psi2 = 0.63;
rho_R = 0.77;
rho_g = 0.98;
rho_z = 0.88;
s_ep = 0.22;
s_eg = 0.72;
s_ez = 0.31;

model;
#  beta = 1/(1 + rA/400);
// IS curve
  y = y(+1) - (1/tau)*(R - pi(+1) - z(+1)) + g - g(+1);
// Phillips curve
  pi = beta*pi(+1) + kappa*(y - g);
// Monetary policy reaction function
  R = rho_R*R(-1) + (1 - rho_R)*(psi1*pi + psi2*(y - g)) +
                s_ep*ep/100;
// Autocorrelated share of government expenditures
  g = rho_g*g(-1) + s_eg*eg/100;
// Autocorrelated productivity shock
  z = rho_z*z(-1) + s_ez*ez/100;
// Observed GDP growth rate in %
  YGR = gammaQ + 100*(y - y(-1) + z);
// Observed YoY inflation rate in %
  INFL = piA + 400*pi;
// Observed interest rate in %
  INT = piA + rA + 4*gammaQ + 400*R;
end;

steady_state_model;
  y = 0;
  pi = 0;
  R = 0;
  g = 0;
  z = 0;
  YGR = gammaQ;
  INFL = piA;
  INT = piA + rA + 4*gammaQ;
end;

steady;

shocks;
  var ep; stderr 1.0;
  var eg; stderr 1.0;
  var ez; stderr 1.0;
  // calibrated measurement errors
  var YGR;  stderr (0.20*0.579923);
  var INFL; stderr (0.20*1.470832);
  var INT;  stderr(0.20*2.237937);
end;

stoch_simul(order=1, irf=0);

varobs YGR, INFL, INT;

histval;
  R(0) = 0.02;
  y(0) = 0.01;
end;

scenario!(name=:pi, value=0.05, period=1, exogenous = :eg);

conditional_forecasting!(periods = 12, forecast_mode=histval);
