///////
// IRBC model from
// Brumm, Krause, Schaab, Scheidegger "Sparse grids for dynamic economic models", 2021.
// https://github.com/SparseGridsForDynamicEcon/SparseGrids_in_econ_handbook/blob/master/doc/sparse_grids_in_econ.pdf
//
// Utility function:
//   u_j(c_j_t) = c_j_t^(1-gamma_j)/(1 - gamma_j)
// Production function:
//   a_j_t * A_t * k_j_{t-1}^kappa 
// Capital adjustment cost:
//   0.5 * phi * k_j_{t-1} * (k_j_t/k_j_{t-1} - 1)^2
//
///////

@#define N=2
@#for j in 1:N
  var c_@{j} k_@{j} a_@{j} y_@{j};
  varexo e_@{j};
  parameters gamma_@{j} eta_@{j} t_@{j};

  a_eis = 0.25;
  b_eis = 1;
  gamma_@{j} = a_eis + @{j}*(b_eis - a_eis)/(@{N}-1);
  eta_@{j} = 0.1;  

@#endfor

var lambda K_Y;
varexo e;
parameters kappa beta delta phi rho A sigE;

// zeta
kappa = 0.36;
// beta
beta  = 0.99;  
delta = 0.025;
// kappa
phi   = 0.5;
// rhoz
rho   = 0.95;
// sigE
sigE = 0.01;
// A_tfp
//A = (1 - beta*(1 - delta))/(kappa*beta);
A = 10;
@#for j in 1:N
  // pareto
  t_@{j} = 1;
@#endfor

change_type(parameters) K_Y;
change_type(var) delta;

//K_Y = 10;

model;
  @#for j in 1:N
    y_@{j} = exp(a_@{j})*A*k_@{j}(-1)^kappa;
    c_@{j} = (lambda/t_@{j})^(-gamma_@{j});
    lambda*(1 + phi*(k_@{j}/k_@{j}(-1) - 1))
      = beta*lambda(+1)*(exp(a_@{j}(+1))*kappa*A*k_@{j}^(kappa - 1)
        + 1 - delta + (phi/2)*(k_@{j}(+1)/k_@{j} - 1)*(k_@{j}(+1)/k_@{j} + 1));
    a_@{j} = rho*a_@{j}(-1) + sigE*(e + e_@{j});
  @#endfor
  y_1
  @#for j in 2:N
  + y_@{j}
  @#endfor
  =
  c_1 + k_1 - (1 - delta)*k_1(-1) + (phi/2)*k_1(-1)*(k_1/k_1(-1) - 1)^2
  @#for j in 2:N
  + c_@{j} + k_@{j} - (1 - delta)*k_@{j}(-1)
             + (phi/2)*k_@{j}(-1)*(k_@{j}/k_@{j}(-1) - 1)^2
  @#endfor
  ;
  K_Y = (k_1
  @#for j in 2:N
  + k_@{j}
  @#endfor
  )/( y_1
  @#for j in 2:N
  + y_@{j}
  @#endfor
  );
end;

initval;
  //  A = 20;
  delta = 0.025;
  //K_Y = 10;
  @#for j in 1:N
    k_@{j} = 1;
    a_@{j} = 0;
    c_@{j} = A - delta;
  y_@{j} = A;
  @#endfor
  lambda = 1;
end;
//delta = 0.03;
homotopy_setup;
  K_Y, 1, 3;
end;

steadystate!(homotopy_steps=1);

