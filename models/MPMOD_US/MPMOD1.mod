//using CSV

var L_GDP, //'Real GDP, 100*Log Level' 
    DL_GDP, //'Real GDP, Log Growth' 
    L_GDP_MP_BAR, //'Trend GDP, Log Level' 
    DL_GDP_MP_BAR, //'Trend GDP Growth (percent)'  
    G_MP, //'Persistent Component in MP GDP Trend Growth' 
    Y_MP, //'Output Gap (percent of trend)' 

    PIE, //'CPI Inflation' 
    PIE_TAR, //'Implicit Target for CPI Inflation' 
    PIE_EXP, //'CPI Inflation Expectations' 

    R1Y,  //'Short Term Nominal Interest Rate'     
    RR1Y,  //'Short Term Real Interest Rate'     
    RR1Y_BAR, //'Equilibrium Short Term Real Interest Rate' 
    RR1Y_GAP; //'Short Term Real Interest Rate Gap' 

varexo RES_L_GDP_MP_BAR, RES_G_MP, RES_Y_MP, RES_PIE, RES_R1Y, RES_RR1Y_BAR, RES_PIE_TAR;


parameters phi1, phi2, phi3, phi4, phi5, theta, growth_ss, lambda1, lambda3, lambda4, beta1,
           alpha1, alpha2, alpha3, alpha4, rho_rr1y_bar, rr1y_ss;

phi1 = 0.7;
phi2 = 0.4;
phi3 = 0.4;
phi4 = 0.3;                
phi5 = 0.8;
theta = 0.3; 
growth_ss = 2.1; 
lambda1 = 0.4; 
lambda3 = 0.1; 
lambda4 = 0.1;           
beta1 = 0.4; 
alpha1 = 0.5; 
alpha2 = 1.5; 
alpha3 = 0.1; 
alpha4 = 2.0;                
rho_rr1y_bar = 0.9; 
rr1y_ss = 0.7;


model;

// GDP Block
  L_GDP = L_GDP_MP_BAR + Y_MP;
  L_GDP_MP_BAR = L_GDP_MP_BAR(-1) + G_MP + RES_L_GDP_MP_BAR;
  G_MP = (1-theta)*G_MP(-1) + theta*growth_ss + RES_G_MP;
  Y_MP = phi1*Y_MP(-1) - phi2*RR1Y_GAP - phi3*RR1Y_GAP(-1) + phi4*RES_G_MP - phi5*RES_L_GDP_MP_BAR + RES_Y_MP;
  DL_GDP = L_GDP - L_GDP(-1);
  DL_GDP_MP_BAR = L_GDP_MP_BAR - L_GDP_MP_BAR(-1);

// Inflation Block
  PIE = lambda1*PIE(+1) + (1-lambda1)*PIE(-1) + lambda3*Y_MP + RES_PIE - lambda4*RES_L_GDP_MP_BAR;
  PIE_TAR = PIE_TAR(-1) + RES_PIE_TAR;
  PIE_EXP = beta1*PIE(+1) + (1-beta1)*PIE(-1);

// Short-Term Interest Rate
  R1Y = alpha1*R1Y(-1) + (1-alpha1)*(RR1Y_BAR + PIE_EXP + alpha2*(PIE - PIE_TAR) + alpha3*Y_MP) + RES_R1Y;
  RR1Y = R1Y - PIE_EXP;
  RR1Y_BAR = rho_rr1y_bar*RR1Y_BAR(-1) + (1-rho_rr1y_bar)*rr1y_ss + RES_RR1Y_BAR;
  RR1Y = RR1Y_BAR + RR1Y_GAP;
end;


steady_state_model;
  Y_MP = 0;
  DL_GDP = growth_ss;
  DL_GDP_MP_BAR = growth_ss;
  G_MP = growth_ss;
  L_GDP = 0;
  L_GDP_MP_BAR = 0;
  PIE_TAR = 0;
  PIE = PIE_TAR;
  PIE_EXP = PIE_TAR;
  RR1Y =  rr1y_ss;
  R1Y = rr1y_ss + PIE_EXP;     
  RR1Y_BAR = rr1y_ss;
  RR1Y_GAP = 0;   
end;

deterministic_trends;
  L_GDP(growth_ss);
  L_GDP_MP_BAR(growth_ss);
end;  

shocks; 
  var RES_L_GDP_MP_BAR; stderr 0.1;
  var RES_G_MP; stderr 0.2;
  var RES_Y_MP; stderr 0.8;
  var RES_PIE; stderr 0.7;
  var RES_PIE_TAR; stderr 0.1;
  var RES_R1Y; stderr 0.4; 
  var RES_RR1Y_BAR; stderr 0.1;
end;

stoch_simul(order=1, nonstationary);


