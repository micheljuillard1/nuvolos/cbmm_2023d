# Central Bank Macroeconimc Modeling Workshop
## Dynare Course
## October 16-20, 2023


## Content
- Macrolanguage - steady state - homotopy
- Forecasting
- Estimation
- Risk
